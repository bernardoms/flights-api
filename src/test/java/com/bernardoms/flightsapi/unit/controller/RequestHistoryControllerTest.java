package com.bernardoms.flightsapi.unit.controller;

import com.bernardoms.flightsapi.controller.ExceptionController;
import com.bernardoms.flightsapi.controller.RequestHistoryController;
import com.bernardoms.flightsapi.model.RequestHistory;
import com.bernardoms.flightsapi.service.RequestHistoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class RequestHistoryControllerTest {
    @InjectMocks
    private RequestHistoryController requestHistoryController;

    @Mock
    private RequestHistoryService requestHistoryService;

    private MockMvc mockMvc;

    private static final String URL_PATH = "/v1/history/requests";


    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(requestHistoryController)
                .setControllerAdvice(ExceptionController.class)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void should_return_ok_with_all_request_history_when_get() throws Exception {
        var values = new String[1];
        values[0] = "value1";

        var history1 = RequestHistory
                .builder()
                .path("/history/requests")
                .params(Map.of("param1", values))
                .build();

        var history2 = RequestHistory
                .builder()
                .path("/history/requests")
                .params(Map.of("param1", values))
                .build();

        when(requestHistoryService.getAll(any(Pageable.class))).thenReturn(new PageImpl<>(Arrays.asList(history1, history2)));

        mockMvc.perform(get(URL_PATH).param("page", "0").param("size", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].path", is("/history/requests")));
    }

    @Test
    void should_return_no_content_when_deleting_all_request_histories() throws Exception {
        mockMvc.perform(delete(URL_PATH))
                .andExpect(status().isNoContent());
    }
}
