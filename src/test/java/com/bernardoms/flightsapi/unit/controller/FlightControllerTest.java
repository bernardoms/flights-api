package com.bernardoms.flightsapi.unit.controller;

import com.bernardoms.flightsapi.controller.ExceptionController;
import com.bernardoms.flightsapi.controller.FlightController;
import com.bernardoms.flightsapi.model.*;
import com.bernardoms.flightsapi.service.AverageFlightService;
import com.bernardoms.flightsapi.service.FlightService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith(MockitoExtension.class)
class FlightControllerTest {
    @InjectMocks
    private FlightController flightController;

    @Mock
    private FlightService flightService;

    @Mock
    private AverageFlightService averageFlightService;

    private MockMvc mockMvc;

    private static final String URL_PATH = "/v1/flights";

    @BeforeEach
    void setUp() {
        mockMvc = standaloneSetup(flightController)
                .setControllerAdvice(ExceptionController.class)
                .build();
    }

    @Test
    void should_return_ok_with_flights_with_all_params() throws Exception {
        var flight = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 20.0d, "2", 30.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(40).build();

        var flightResponse = FlightClientResponse
                .builder()
                .currency("EUR")
                .data(Collections.singletonList(flight))
                .build();

        when(flightService.getFlights(any(FlightClientRequest.class))).thenReturn(flightResponse);

        mockMvc.perform(get(URL_PATH)
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("cur", "EUR")
                .param("flyFrom", "LIS")
                .param("flyTo", "OPO"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.currency", is("EUR")))
                .andExpect(jsonPath("$.data[0].flyFrom", is("LIS")))
                .andExpect(jsonPath("$.data[0].flyTo", is("OPO")))
                .andExpect(jsonPath("$.data[0].airlines", is(Collections.singletonList("TP"))))
                .andExpect(jsonPath("$.data[0].distance", is(276.0)))
                .andExpect(jsonPath("$.data[0].price", is(40.0)))
                .andExpect(jsonPath("$.data[0].fly_duration", is("1h 0m")));
    }

    @Test
    void should_return_bad_request_when_request_flight_is_missing_required_params() throws Exception {
        mockMvc.perform(get(URL_PATH))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_4xx_when_has_a_client_exception_recovering_flights() throws Exception {
        when(flightService.getFlights(any(FlightClientRequest.class))).thenThrow(new HttpClientErrorException(HttpStatus.CONFLICT));
        mockMvc.perform(get(URL_PATH)
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("cur", "EUR")
                .param("flyFrom", "LIS")
                .param("flyTo", "OPO"))
                .andExpect(status().isConflict());
    }

    @Test
    void should_return_5xx_when_has_a_server_exception_recovering_flights() throws Exception {
        when(flightService.getFlights(any(FlightClientRequest.class))).thenThrow(new HttpServerErrorException(HttpStatus.BAD_GATEWAY));
        mockMvc.perform(get(URL_PATH)
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("cur", "EUR")
                .param("flyFrom", "LIS")
                .param("flyTo", "OPO"))
                .andExpect(status().isBadGateway());
    }

    @Test
    void should_return_internal_exception_when_is_an_error_on_service() throws Exception {
        when(flightService.getFlights(any(FlightClientRequest.class))).thenThrow(new RuntimeException(""));
        mockMvc.perform(get(URL_PATH)
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("cur", "EUR")
                .param("flyFrom", "LIS")
                .param("flyTo", "OPO"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void should_return_ok_with_average_flight_response() throws Exception {
        var averagleFlightToResponse = AverageFlightToAirport.builder()
                .airportCode("OPO")
                .bagsPriceAverage(Map.of("1", BigDecimal.valueOf(20), "2", BigDecimal.valueOf(30)))
                .currency(Currency.EUR)
                .priceAverage(BigDecimal.valueOf(200))
                .build();
        var averageFlightFromResponse = AverageFlightFromAirport.builder()
                .airportCode("LIS")
                .to(Collections.singletonList(averagleFlightToResponse))
                .build();
        var averageFlightResponse = AverageFlightResponse.builder()
                .from(Collections.singletonList(averageFlightFromResponse))
                .build();

        when(averageFlightService.getAveragePriceFromFlights(any(FlightClientRequest.class))).thenReturn(averageFlightResponse);

        mockMvc.perform(get(URL_PATH + "/avg")
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("cur", "EUR")
                .param("flyFrom", "LIS")
                .param("flyTo", "OPO"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.from[0].airportCode", is("LIS")))
                .andExpect(jsonPath("$.from[0].to[0].airportCode", is("OPO")))
                .andExpect(jsonPath("$.from[0].to[0].currency", is("EUR")))
                .andExpect(jsonPath("$.from[0].to[0].priceAverage", is(200)))
                .andExpect(jsonPath("$.from[0].to[0].bagsPriceAverage", is(Map.of("1", 20, "2", 30))));
    }
}
