package com.bernardoms.flightsapi.unit.service;

import com.bernardoms.flightsapi.model.Currency;
import com.bernardoms.flightsapi.model.Flight;
import com.bernardoms.flightsapi.model.FlightClientRequest;
import com.bernardoms.flightsapi.model.FlightClientResponse;
import com.bernardoms.flightsapi.service.AverageFlightServiceImpl;
import com.bernardoms.flightsapi.service.FlightService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AverageFlightServiceImplTest {
    @Mock
    private FlightService flightService;
    @InjectMocks
    private AverageFlightServiceImpl averageFlightService;

    @Test
    void should_calculate_average_for_flights_out_from_same_city_and_arriving_at_same_city() {
        var flight1 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 40.0d, "2",30.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(40).build();

        var flight2 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 40.0d, "2",30.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(60).build();

        var flight3 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 70.0d, "2",30.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(200).build();

        var flightRequest = FlightClientRequest.builder()
                .curr(Currency.EUR)
                .flyFrom("LIS")
                .flyTo("OPO")
                .select_airlines("TAP")
                .from(LocalDate.of(2020,10,15))
                .to(LocalDate.of(2020,11,15))
                .build();

        when(flightService.getFlights(flightRequest)).thenReturn(FlightClientResponse.builder()
                .currency("EUR")
                .data(Arrays.asList(flight1, flight2, flight3)).build());

        var averagePriceFromFlights = averageFlightService.getAveragePriceFromFlights(flightRequest);

        assertEquals(BigDecimal.valueOf(100.00).setScale(2, RoundingMode.HALF_UP), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("lis")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("OPO")).findFirst().orElseThrow().getPriceAverage());

        assertEquals(Map.of("1", BigDecimal.valueOf(50).setScale(2, RoundingMode.HALF_UP),"2",BigDecimal.valueOf(30).setScale(2, RoundingMode.HALF_UP)), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("lis")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("OPO")).findFirst().orElseThrow().getBagsPriceAverage());
    }

    @Test
    void should_calculate_average_for_when_there_is_no_bag_consider_price_zero() {
        var flight1 = Flight.builder().airlines(Collections.singletonList("TP"))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(40).build();

        var flight2 = Flight.builder().airlines(Collections.singletonList("TP"))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(60).build();

        var flight3 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 70.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(200).build();

        var flightRequest = FlightClientRequest.builder()
                .curr(Currency.EUR)
                .flyFrom("LIS")
                .flyTo("OPO")
                .select_airlines("TAP")
                .from(LocalDate.of(2020,10,15))
                .to(LocalDate.of(2020,11,15))
                .build();

        when(flightService.getFlights(flightRequest)).thenReturn(FlightClientResponse.builder()
                .currency("EUR")
                .data(Arrays.asList(flight1, flight2, flight3)).build());

        var averagePriceFromFlights = averageFlightService.getAveragePriceFromFlights(flightRequest);

        assertEquals(BigDecimal.valueOf(100).setScale(2, RoundingMode.HALF_UP), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("lis")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("OPO")).findFirst().orElseThrow().getPriceAverage());

        assertEquals(Map.of("1", BigDecimal.valueOf(70).setScale(2, RoundingMode.HALF_UP),"2",BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP)), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("lis")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("OPO")).findFirst().orElseThrow().getBagsPriceAverage());
    }

    @Test
    void should_calculate_average_for_flights_out_from_different_city_and_arriving_at_different_city() {
        var flight1 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 40.0d, "2",30.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(40).build();

        var flight2 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 60.0d, "2",30.0d))
                .flyFrom("LIS")
                .flyTo("JFK")
                .cityFrom("Lisbon")
                .cityTo("New York")
                .distance(276)
                .flyDuration("12h 0m")
                .price(60).build();

        var flight3 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 70.0d, "2",30.0d))
                .flyFrom("GAR")
                .flyTo("JFK")
                .cityFrom("Brazil")
                .cityTo("New York")
                .distance(276)
                .flyDuration("12h 0m")
                .price(200).build();

        var flight4 = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 70.0d, "2",30.0d))
                .flyFrom("CHL")
                .flyTo("JFK")
                .cityFrom("Chicago")
                .cityTo("Nova York")
                .distance(276)
                .flyDuration("5h 0m")
                .price(200).build();

        var flightRequest = FlightClientRequest.builder()
                .curr(Currency.EUR)
                .flyFrom("LIS,GAR,CHL")
                .flyTo("OPO,JFK")
                .select_airlines("TAP")
                .from(LocalDate.of(2020,10,15))
                .to(LocalDate.of(2020,11,15))
                .build();

        when(flightService.getFlights(flightRequest)).thenReturn(FlightClientResponse.builder()
                .currency("EUR")
                .data(Arrays.asList(flight1, flight2, flight3,flight4)).build());

        var averagePriceFromFlights = averageFlightService.getAveragePriceFromFlights(flightRequest);

        assertEquals(BigDecimal.valueOf(40).setScale(2, RoundingMode.HALF_UP), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("lis")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("OPO")).findFirst().orElseThrow().getPriceAverage());

        assertEquals(Map.of("1", BigDecimal.valueOf(40).setScale(2, RoundingMode.HALF_UP),"2",BigDecimal.valueOf(30).setScale(2, RoundingMode.HALF_UP)), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("lis")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("OPO")).findFirst().orElseThrow().getBagsPriceAverage());

        assertEquals(BigDecimal.valueOf(60).setScale(2, RoundingMode.HALF_UP), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("lis")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("JFK")).findFirst().orElseThrow().getPriceAverage());


        assertEquals(Map.of("1", BigDecimal.valueOf(70).setScale(2, RoundingMode.HALF_UP),"2",BigDecimal.valueOf(30).setScale(2, RoundingMode.HALF_UP)), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("GAR")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("JFK")).findFirst().orElseThrow().getBagsPriceAverage());


        assertEquals(BigDecimal.valueOf(200).setScale(2, RoundingMode.HALF_UP), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("GAR")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("JFK")).findFirst().orElseThrow().getPriceAverage());


        assertEquals(BigDecimal.valueOf(200).setScale(2, RoundingMode.HALF_UP), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("CHL")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("JFK")).findFirst().orElseThrow().getPriceAverage());

        assertEquals(Map.of("1", BigDecimal.valueOf(70).setScale(2, RoundingMode.HALF_UP),"2",BigDecimal.valueOf(30).setScale(2, RoundingMode.HALF_UP)), averagePriceFromFlights
                .getFrom()
                .stream()
                .filter(f->f.getAirportCode().equalsIgnoreCase("GAR")).findFirst().orElseThrow().getTo().stream()
                .filter(t->t.getAirportCode().equalsIgnoreCase("JFK")).findFirst().orElseThrow().getBagsPriceAverage());
    }

}
