package com.bernardoms.flightsapi.unit.service;

import com.bernardoms.flightsapi.config.FlightClientConfig;
import com.bernardoms.flightsapi.model.Currency;
import com.bernardoms.flightsapi.model.Flight;
import com.bernardoms.flightsapi.model.FlightClientRequest;
import com.bernardoms.flightsapi.model.FlightClientResponse;
import com.bernardoms.flightsapi.service.FlightServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FlightServiceImplTest {
    @InjectMocks
    private FlightServiceImpl flightService;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private FlightClientConfig flightClientConfig;

    @Test
    void should_mount_all_query_param_and_return_flights() {
        UriComponentsBuilder uriComponents =
                UriComponentsBuilder.fromHttpUrl("http://test.com/flights?date_from=15/10/2020&date_to=15/11/2020&fly_from=LIS&fly_to=OPO&partner=picky&curr=EUR&select_airlines=TAP");

        var flight = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 20.0d, "2",30.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(40).build();

        when(restTemplate.getForObject(uriComponents.build().toUri(), FlightClientResponse.class))
                .thenReturn(FlightClientResponse
                        .builder()
                        .currency("EUR")
                        .data(Collections.singletonList(flight))
                        .build());
        when(flightClientConfig.getEndpoint()).thenReturn("http://test.com/flights");
        when(flightClientConfig.getPartner()).thenReturn("picky");

        var flightClientResponse = flightService.getFlights(FlightClientRequest.builder()
                .curr(Currency.EUR)
                .flyFrom("LIS")
                .flyTo("OPO")
                .select_airlines("TAP")
                .from(LocalDate.of(2020,10,15))
                .to(LocalDate.of(2020,11,15))
                .build());

        assertEquals(Currency.EUR.name(), flightClientResponse.getCurrency());
        assertEquals(flight.getAirlines(), flightClientResponse.getData().stream().findFirst().orElseThrow().getAirlines());
        assertEquals(flight.getBagPrice(), flightClientResponse.getData().stream().findFirst().orElseThrow().getBagPrice());
        assertEquals(flight.getFlyFrom(), flightClientResponse.getData().stream().findFirst().orElseThrow().getFlyFrom());
        assertEquals(flight.getCityTo(), flightClientResponse.getData().stream().findFirst().orElseThrow().getCityTo());
        assertEquals(flight.getFlyTo(), flightClientResponse.getData().stream().findFirst().orElseThrow().getFlyTo());
        assertEquals(flight.getDistance(), flightClientResponse.getData().stream().findFirst().orElseThrow().getDistance());
        assertEquals(flight.getPrice(), flightClientResponse.getData().stream().findFirst().orElseThrow().getPrice());
        assertEquals(flight.getFlyDuration(), flightClientResponse.getData().stream().findFirst().orElseThrow().getFlyDuration());
    }

    @Test
    void should_mount_only_required_params() {
        UriComponentsBuilder uriComponents =
                UriComponentsBuilder.fromHttpUrl("http://test.com/flights?date_from=15/10/2020&date_to=15/11/2020&fly_from=LIS&fly_to=OPO&partner=picky");

        var flight = Flight.builder().airlines(Collections.singletonList("TP"))
                .bagPrice(Map.of("1", 20.0d, "2",30.0d))
                .flyFrom("LIS")
                .flyTo("OPO")
                .cityFrom("Lisbon")
                .cityTo("Porto")
                .distance(276)
                .flyDuration("1h 0m")
                .price(40).build();

        when(restTemplate.getForObject(uriComponents.build().toUri(), FlightClientResponse.class))
                .thenReturn(FlightClientResponse
                        .builder()
                        .currency("EUR")
                        .data(Collections.singletonList(flight))
                        .build());
        when(flightClientConfig.getEndpoint()).thenReturn("http://test.com/flights");
        when(flightClientConfig.getPartner()).thenReturn("picky");

        var flightClientResponse = flightService.getFlights(FlightClientRequest.builder()
                .flyFrom("LIS")
                .flyTo("OPO")
                .from(LocalDate.of(2020,10,15))
                .to(LocalDate.of(2020,11,15))
                .build());

        assertEquals(Currency.EUR.name(), flightClientResponse.getCurrency());
        assertEquals(flight.getAirlines(), flightClientResponse.getData().stream().findFirst().orElseThrow().getAirlines());
        assertEquals(flight.getBagPrice(), flightClientResponse.getData().stream().findFirst().orElseThrow().getBagPrice());
        assertEquals(flight.getFlyFrom(), flightClientResponse.getData().stream().findFirst().orElseThrow().getFlyFrom());
        assertEquals(flight.getCityTo(), flightClientResponse.getData().stream().findFirst().orElseThrow().getCityTo());
        assertEquals(flight.getCityTo(), flightClientResponse.getData().stream().findFirst().orElseThrow().getCityTo());
        assertEquals(flight.getDistance(), flightClientResponse.getData().stream().findFirst().orElseThrow().getDistance());
        assertEquals(flight.getPrice(), flightClientResponse.getData().stream().findFirst().orElseThrow().getPrice());
        assertEquals(flight.getFlyDuration(), flightClientResponse.getData().stream().findFirst().orElseThrow().getFlyDuration());
    }
}
