package com.bernardoms.flightsapi.unit.service;

import com.bernardoms.flightsapi.model.RequestHistory;
import com.bernardoms.flightsapi.repository.RequestHistoryRepository;
import com.bernardoms.flightsapi.service.RequestHistoryServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RequestHistoryServiceImplTest {
    @InjectMocks
    private RequestHistoryServiceImpl requestHistoryService;
    @Mock
    private RequestHistoryRepository requestHistoryRepository;

    @Test
    void should_save_request_history() {
        String[] values = new String[1];
        values[0] = "value1";

        var requestHistory = RequestHistory
                .builder()
                .path("/history/requests")
                .params(Map.of("param1", values))
                .build();

        requestHistoryService.save(requestHistory);

        verify(requestHistoryRepository, times(1)).save(requestHistory);
    }

    @Test
    void should_return_page_of_request_history() {
        String[] values = new String[1];
        values[0] = "value1";

        var history1 = RequestHistory
                .builder()
                .path("/history/requests")
                .params(Map.of("param1", values))
                .build();

        var history2 = RequestHistory
                .builder()
                .path("/history/requests")
                .params(Map.of("param1", values))
                .build();

        when(requestHistoryRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Arrays.asList(history1, history2)));

        var histories = requestHistoryService.getAll(Pageable.unpaged());

        assertEquals(2, histories.getTotalElements());

        assertEquals(1, histories.getTotalPages());
    }

    @Test
    void should_delete_request_history() {
        requestHistoryService.deleteAll();
        verify(requestHistoryRepository, timeout(1)).deleteAll();
    }
}
