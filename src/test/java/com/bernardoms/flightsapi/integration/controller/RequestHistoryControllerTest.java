package com.bernardoms.flightsapi.integration.controller;

import com.bernardoms.flightsapi.integration.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class RequestHistoryControllerTest extends IntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    private static final String URL_PATH = "/v1/history/requests";

    @Test
    void should_return_ok_with_all_request_history_when_get() throws Exception {
        mockMvc.perform(get(URL_PATH).param("page", "0").param("size", "2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].path", is("/flights")))
                .andExpect(jsonPath("$.content[1].path", is("/flights/avg")));
    }

    @Test
    void should_return_no_content_when_deleting_all_request_histories() throws Exception {
        mockMvc.perform(delete(URL_PATH))
                .andExpect(status().isNoContent());
    }
}
