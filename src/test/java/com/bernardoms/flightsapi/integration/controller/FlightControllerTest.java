package com.bernardoms.flightsapi.integration.controller;

import com.bernardoms.flightsapi.integration.IntegrationTest;
import com.bernardoms.flightsapi.model.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class FlightControllerTest extends IntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private static final String URL_PATH = "/v1/flights";

    @Test
    void should_get_return_ok_with_all_flights() throws Exception {
        mockMvc.perform(get(URL_PATH)
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("cur", "GBP")
                .param("flyFrom", "LIS")
                .param("flyTo", "OPO"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.currency", is("GBP")))
                .andExpect(jsonPath("$.data[0].flyFrom", is("LIS")))
                .andExpect(jsonPath("$.data[0].flyTo", is("OPO")))
                .andExpect(jsonPath("$.data[0].airlines", is(Collections.singletonList("TP"))))
                .andExpect(jsonPath("$.data[0].distance", is(276.74)))
                .andExpect(jsonPath("$.data[0].price", is(40.0)))
                .andExpect(jsonPath("$.data[0].fly_duration", is("1h 0m")));
    }

    @Test
    void should_return_ok_with_average_flight_response() throws Exception {
        mockMvc.perform(get(URL_PATH + "/avg")
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("cur", "GBP")
                .param("flyFrom", "LIS")
                .param("flyTo", "OPO"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.from[0].airportCode", is("LIS")))
                .andExpect(jsonPath("$.from[0].to[0].airportCode", is("OPO")))
                .andExpect(jsonPath("$.from[0].to[0].currency", is("EUR")))
                .andExpect(jsonPath("$.from[0].to[0].priceAverage", is(99.81)))
                .andExpect(jsonPath("$.from[0].to[0].bagsPriceAverage", is(Map.of("1", 60.87, "2", 181.67))));
    }

    @Test
    void should_return_ok_with_average_flight_response_flying_from_multiple_places() throws Exception {
        mockMvc.perform(get(URL_PATH + "/avg")
                .param("from", "13/12/2020")
                .param("to", "24/12/2020")
                .param("select_airlines", "TP")
                .param("curr", "GBP")
                .param("flyFrom", "LIS,OPO")
                .param("flyTo", "JFK"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.from[0].airportCode", is("LIS")))
                .andExpect(jsonPath("$.from[0].to[0].airportCode", is("JFK")))
                .andExpect(jsonPath("$.from[0].to[0].currency", is("GBP")))
                .andExpect(jsonPath("$.from[0].to[0].priceAverage", is(304.67)))
                .andExpect(jsonPath("$.from[0].to[0].bagsPriceAverage", is(Map.of("1", 112.5, "2", 247.5))))
                .andExpect(jsonPath("$.from[1].airportCode", is("OPO")))
                .andExpect(jsonPath("$.from[1].to[0].airportCode", is("JFK")))
                .andExpect(jsonPath("$.from[1].to[0].currency", is("GBP")))
                .andExpect(jsonPath("$.from[1].to[0].priceAverage", is(288.51)))
                .andExpect(jsonPath("$.from[1].to[0].bagsPriceAverage", is(Map.of("1", 184.68, "2", 463.34))));
    }
}
