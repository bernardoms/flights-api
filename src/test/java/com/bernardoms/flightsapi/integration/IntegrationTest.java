package com.bernardoms.flightsapi.integration;

import com.bernardoms.flightsapi.model.RequestHistory;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Map;

@SpringBootTest
public abstract class IntegrationTest {


    private static boolean alreadySaved = false;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeEach
    public void setUp() {
        String[] values = new String[1];
        values[0] = "value1";
        mongoTemplate
                .save(RequestHistory
                        .builder()
                        .path("/flights")
                        .params(Map.of("param1", values)).build());
        mongoTemplate
                .save(RequestHistory
                        .builder()
                        .path("/flights/avg")
                        .params(Map.of("param1", values)).build());
        alreadySaved = true;
    }
}
