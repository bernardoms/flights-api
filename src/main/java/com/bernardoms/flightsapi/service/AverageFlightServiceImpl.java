package com.bernardoms.flightsapi.service;

import com.bernardoms.flightsapi.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AverageFlightServiceImpl implements AverageFlightService {
    private static final String BAG1 = "1";
    private static final String BAG2 = "2";

    private final FlightService flightService;

    public AverageFlightResponse getAveragePriceFromFlights(FlightClientRequest flightClientRequest) {
        List<AverageFlightFromAirport> averageFlights = new ArrayList<>();
        var flights = flightService.getFlights(flightClientRequest);
        log.info("getting average sum for {} flights", flights.getData().size());
        var flightsGroupedByFlyFrom = flights.getData().stream().collect(Collectors.groupingBy(Flight::getFlyFrom, Collectors.groupingBy(Flight::getFlyTo)));

        flightsGroupedByFlyFrom.forEach((key, value) -> {
            var averageFlightFromAirport = new AverageFlightFromAirport();

            averageFlightFromAirport.setAirportCode(key);
            List<AverageFlightToAirport> toAirports = new ArrayList<>();

            for (Map.Entry<String, List<Flight>> entry : value.entrySet()) {
                var averageFlightToAirport = new AverageFlightToAirport();

                var averagePrice = entry.getValue().stream().map(Flight::getPrice).mapToDouble(w -> w).average().orElse(0);
                var averageBag1 = entry.getValue().stream().filter(f -> f.getBagPrice() != null).map(Flight::getBagPrice).filter(w -> w.get(BAG1) != null).mapToDouble(w -> w.get(BAG1)).average().orElse(0);
                var averageBag2 = entry.getValue().stream().filter(f -> f.getBagPrice() != null).map(Flight::getBagPrice).filter(w -> w.get(BAG2) != null).mapToDouble(w -> w.get(BAG2)).average().orElse(0);

                averageFlightToAirport.setBagsPriceAverage(Map.of(BAG1, BigDecimal.valueOf(averageBag1).setScale(2, RoundingMode.HALF_UP), BAG2, BigDecimal.valueOf(averageBag2).setScale(2, RoundingMode.HALF_UP)));

                averageFlightToAirport.setPriceAverage(BigDecimal.valueOf(averagePrice).setScale(2, RoundingMode.HALF_UP));
                averageFlightToAirport.setCurrency(flightClientRequest.getCurr() != null ? flightClientRequest.getCurr() : Currency.EUR);
                averageFlightToAirport.setAirportCode(entry.getKey());
                toAirports.add(averageFlightToAirport);
                averageFlightFromAirport.setTo(toAirports);
            }
            averageFlights.add(averageFlightFromAirport);
        });
        return AverageFlightResponse.builder().from(averageFlights).build();
    }
}
