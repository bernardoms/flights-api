package com.bernardoms.flightsapi.service;

import com.bernardoms.flightsapi.model.RequestHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RequestHistoryService {
    void save(RequestHistory requestHistory);
    Page<RequestHistory> getAll(Pageable pageable);
    void deleteAll();
}
