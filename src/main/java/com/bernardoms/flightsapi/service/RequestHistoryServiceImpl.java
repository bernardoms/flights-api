package com.bernardoms.flightsapi.service;

import com.bernardoms.flightsapi.model.RequestHistory;
import com.bernardoms.flightsapi.repository.RequestHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RequestHistoryServiceImpl implements RequestHistoryService {
    private final RequestHistoryRepository requestHistoryRepository;

    @Override
    public void save(RequestHistory requestHistory) {
        requestHistoryRepository.save(requestHistory);
    }

    public Page<RequestHistory> getAll(Pageable pageable) {
        return requestHistoryRepository.findAll(pageable);
    }

    @Override
    public void deleteAll() {
        requestHistoryRepository.deleteAll();
    }
}
