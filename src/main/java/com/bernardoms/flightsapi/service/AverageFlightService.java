package com.bernardoms.flightsapi.service;

import com.bernardoms.flightsapi.model.AverageFlightResponse;
import com.bernardoms.flightsapi.model.FlightClientRequest;

public interface AverageFlightService {
    AverageFlightResponse getAveragePriceFromFlights(FlightClientRequest flightClientRequest);
}
