package com.bernardoms.flightsapi.service;

import com.bernardoms.flightsapi.config.FlightClientConfig;
import com.bernardoms.flightsapi.model.FlightClientResponse;
import com.bernardoms.flightsapi.model.FlightClientRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {
    private final RestTemplate restTemplate;
    private final FlightClientConfig flightClientConfig;
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");


    private URI mountRequestParameter(FlightClientRequest flightClientRequest) {
        UriComponentsBuilder uriComponents =
                UriComponentsBuilder.fromHttpUrl(flightClientConfig.getEndpoint());

        uriComponents.queryParam("date_from", flightClientRequest.getFrom().format(DATE_TIME_FORMATTER));
        uriComponents.queryParam("date_to", flightClientRequest.getTo().format(DATE_TIME_FORMATTER));
        uriComponents.queryParam("fly_from", flightClientRequest.getFlyFrom());
        uriComponents.queryParam("fly_to", flightClientRequest.getFlyTo());
        uriComponents.queryParam("partner", flightClientConfig.getPartner());

        if (!StringUtils.isEmpty(flightClientRequest.getCurr())) {
            uriComponents.queryParam("curr", flightClientRequest.getCurr());
        }
        if (!StringUtils.isEmpty(flightClientRequest.getSelect_airlines())) {
            uriComponents.queryParam("select_airlines", flightClientRequest.getSelect_airlines());
        }
        return uriComponents.build().toUri();
    }
    @Cacheable(cacheNames = "flights")
    public FlightClientResponse getFlights(FlightClientRequest flightClientRequest) {
        return restTemplate.getForObject(mountRequestParameter(flightClientRequest), FlightClientResponse.class);
    }
}
