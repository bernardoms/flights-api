package com.bernardoms.flightsapi.service;

import com.bernardoms.flightsapi.model.FlightClientResponse;
import com.bernardoms.flightsapi.model.FlightClientRequest;

public interface FlightService {
    FlightClientResponse getFlights(FlightClientRequest flightClientRequest);
}
