package com.bernardoms.flightsapi.controller;

import com.bernardoms.flightsapi.model.RequestHistory;
import com.bernardoms.flightsapi.service.RequestHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/history/requests")
@RequiredArgsConstructor
public class RequestHistoryController {
    private final RequestHistoryService requestHistoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<RequestHistory> getAllRequestHistory(Pageable pageable) {
        return requestHistoryService.getAll(pageable);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAllRequestHistory() {
        requestHistoryService.deleteAll();
    }
}
