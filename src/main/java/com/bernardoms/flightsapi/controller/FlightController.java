package com.bernardoms.flightsapi.controller;

import com.bernardoms.flightsapi.model.AverageFlightResponse;
import com.bernardoms.flightsapi.model.FlightClientResponse;
import com.bernardoms.flightsapi.model.FlightClientRequest;
import com.bernardoms.flightsapi.service.AverageFlightService;
import com.bernardoms.flightsapi.service.FlightService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/flights")
@RequiredArgsConstructor
public class FlightController {
    private final FlightService flightService;
    private final AverageFlightService averageFlightService;

    @GetMapping(produces = "application/json", headers="Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public FlightClientResponse getFlights(@Validated FlightClientRequest flightClientRequest) {
        return flightService.getFlights(flightClientRequest);
    }

    @GetMapping(value = "/avg", produces = "application/json", headers="Accept=application/json")
    @ResponseStatus(HttpStatus.OK)
    public AverageFlightResponse getAverageValueFromFlights(@Validated FlightClientRequest flightClientRequest) {
        return averageFlightService.getAveragePriceFromFlights(flightClientRequest);
    }
}
