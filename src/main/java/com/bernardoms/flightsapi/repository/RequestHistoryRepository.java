package com.bernardoms.flightsapi.repository;

import com.bernardoms.flightsapi.model.RequestHistory;
import org.bson.types.ObjectId;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RequestHistoryRepository extends PagingAndSortingRepository<RequestHistory, ObjectId> {
}
