package com.bernardoms.flightsapi.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Map;

@Document(collection = "requestHistory")
@Getter
@Setter
@Builder
public class RequestHistory {
    private String path;
    private Map<String, String[]> params;
    @CreatedDate
    private LocalDateTime createdDate;
}
