package com.bernardoms.flightsapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;
import java.util.Map;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Flight {
    @JsonProperty("fly_duration")
    private String flyDuration;
    private String flyFrom;
    private String cityFrom;
    private String flyTo;
    private String cityTo;
    private String nightsInDest;
    private double price;
    @JsonProperty("bags_price")
    private Map<String, Double> bagPrice;
    private double distance;
    private List<String> airlines;
}
