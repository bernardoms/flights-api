package com.bernardoms.flightsapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FlightClientRequest {
    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate from;
    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate to;
    private String select_airlines;
    private Currency curr;
    private String flyFrom;
    @NotBlank
    private String flyTo;

}
