package com.bernardoms.flightsapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AverageFlightToAirport {
    private String airportCode;
    private Currency currency;
    private BigDecimal priceAverage;
    private Map<String, BigDecimal> bagsPriceAverage;
}
