package com.bernardoms.flightsapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightClientResponse {
    private String currency;
    @JsonProperty("currency_rate")
    private String currencyRate;
    private List<Flight> data;
}
