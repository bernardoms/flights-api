package com.bernardoms.flightsapi.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class AverageFlightResponse {
    List<AverageFlightFromAirport> from;
}
