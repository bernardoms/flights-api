package com.bernardoms.flightsapi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AverageFlightFromAirport {
    private String airportCode;
    private List<AverageFlightToAirport> to;
}
